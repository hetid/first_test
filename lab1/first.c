#include <stdio.h>
#include <unistd.h>

int main ()
{
	int MyTik=0;
	puts ("Таймер на 5 секунд\n");
	while (MyTik<5)
	{
		printf("Осталось %d секунд!\n", 5-MyTik);
		sleep (1);
		MyTik++;
	}
	printf("У вас не осталось секунд...\n");
	return (0);
}