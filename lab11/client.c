#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#define PORT 1488
#define PIECE 6
#define countw 32

int rng(){
	int var = 0;
	srand(time(NULL));
	int count = 2;
	for (int i = 0; i < count; ++i) {
		if (var == 0) {
			var = rand()%2;
		}
	}
	return(var);
}

int main(int argc, char const *argv[]){
	//int countw = atoi(argv[1]);
	char param = argv[1][0];

	int Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	struct sockaddr_in Sockaddr;
	Sockaddr.sin_family = AF_INET;
	Sockaddr.sin_port = htons(PORT);
	Sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	connect(Socket, (struct sockaddr*)(&Sockaddr), sizeof(Sockaddr));

	send(Socket, &param, 1, MSG_NOSIGNAL);

	if (param == 'w'){	//код клиента-рабочего
		char buff[] = "piece";
			//RNG
		int var = rng();
		char buf = 'n';
		switch(var){
			case 0:
				send(Socket, &buf, 1, MSG_NOSIGNAL);
				break;
			case 1:
				send(Socket, &buff, PIECE, MSG_NOSIGNAL);
				break;
		}
	} else if (param == 'k'){	//код клиента-контролера
	//	sleep(5);
		char bufconv[countw][PIECE];
		recv(Socket, &bufconv, sizeof(char*)*countw, MSG_NOSIGNAL);
		//анализ
		int n = 1;
		for (int i = 0; i < countw; ++i){
			if (bufconv[i][0] == 'n'){
				n = 0;
			}
		}
		send(Socket, &n, sizeof(int), MSG_NOSIGNAL);
	}
	return 0;
}