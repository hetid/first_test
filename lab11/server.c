#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define PORT 1488
#define PIECE 6
#define MAXWORK 16
int main(int argc, char const *argv[]){
	//int countw = atoi(argv[1]);
	char conv[MAXWORK][PIECE];

	int counter = 0; // счётчик подключений

	int nullsocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in Sockaddr;
	Sockaddr.sin_family = AF_INET;
	Sockaddr.sin_port = htons(PORT);
	Sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	bind(nullsocket, (struct sockaddr*)(&Sockaddr), sizeof(Sockaddr));

	listen(nullsocket, SOMAXCONN);

	while(1){
		int truesocket = accept(nullsocket, 0, 0);							//deskr
								//piece
		char buf[PIECE] = {0, 0, 0, 0, 0, 0}; 
		char symb = 0;
		recv(truesocket, &symb, 1, MSG_NOSIGNAL);							//опозновательный знак клиента
		if (symb == 'w'){
			recv(truesocket, &buf, PIECE-1, MSG_NOSIGNAL);					//принимать деталь или 0
			//записать в массив
			strcpy(conv[counter], buf);
			printf("%s\n", conv[counter]);
			shutdown(truesocket, SHUT_RDWR);
			close(truesocket);
			counter++;
		} else if (symb == 'k'){
		//	char **pointer = conv;
			send(truesocket, &conv, sizeof(char*)*MAXWORK, MSG_NOSIGNAL);	//отправлять конвейер
			int n;
			recv(truesocket, &n, sizeof(int), MSG_NOSIGNAL);					//принимать отчёт от контролёра
			if (n == 0){
				printf("От контролёра поступила жалоба на бригаду!\n");
			} else if (n == 1){
				printf("От контролёра не поступило жалоб на бригаду!\n");				
			}
			for (int i = 0; i < MAXWORK; ++i) {
				conv[i][0] = 0;
			}
			counter = 0;
			shutdown(truesocket, SHUT_RDWR);
			close(truesocket);

		}
	}
	return 0;
}