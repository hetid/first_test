#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/*	argv[1] - количесво рабочих */
int rng(){
	int var;
	srand(getpid());
	var = rand()%2;
	switch(var){
		case 0:
			var = rand()%2;
			switch(var){
				case 0:
					var = rand()%2;
					switch(var){
						case 0:
							return(0);
							break;
						case 1:
							return(1);
							break;
					}
					break;
				case 1:
					return(1);
					break;
			}
			break;
		case 1:
			return(1);
			break;
	}
	return(0);
}

int main(int argc, char* argv[]) {
	//FILE *fp, *pf;
	int countw = atoi(argv[1]);
	int fd[countw+1][2]; // массивы дескрипторов для пайпов
	pid_t pids[countw], conv[countw];
	int stat;
	for (int i = 0; i < countw; ++i) {
		pipe(fd[i]);
		pids[i] = fork();
		if (pids[i] == 0) {
			close(fd[i][0]); //запрещает чтение из 0-конца в дочернем процессе
			int var = rng();
			pid_t a = 0;
			switch(var){
				case 0:
					printf("Рабочий %d [%d] не поместил деталь, жалоба на контролёра!\n", i+1, getpid());
					write(fd[i][1], &var, sizeof(pid_t));
					break;
				case 1:
					a = getpid();
					write(fd[i][1], &a, sizeof(pid_t));
					break;
			}
			exit(0);
		}
	}
	for (int i = 0; i < countw; ++i) {
		waitpid(pids[i], &stat, 0);
		close(fd[i][1]); //запрещает запись в 1-конец в родительском процессе
		read(fd[i][0], &(conv[i]), sizeof(pid_t));
		printf("%d\n", conv[i]);
	}
	//printf("Запускается процесс-контролёр.\n");
	pid_t kpid;
	int kd[2];
	pipe(kd);
	kpid = fork();
	if (kpid == 0) {
			int countk = 0;
			close(kd[0]); //запрещает чтение из 0-конца в дочернем процессе
			for (int i = 0; i < countw; ++i) {
				if (pids[i] == conv[i]) {
					countk++;
				}
			}
			write(kd[1], &countk, sizeof(int));
			exit(0);
		}
	waitpid(kpid, &stat, 0);
	int info = 0;
	close(kd[1]); //запрещает запись в 1-конец в родительском процессе
	read(kd[0], &info, sizeof(int));
	printf("%d\n", info);
	if (info == 0)
	{
		printf("От контролёра поступила жалоба на бригаду.\n");
	}
	else printf("Жалоб на бригаду не поступило.\n");
	//printf("Родительский завершился.\n");
	return 0;
}

/*Бригадный подряд. Родительский процесс создает заданное 
количество дочерних (условных рабочих), которые выкладывают 
на конвейер (в файл, глобальный массив) деталь (например, 
свой идентификатор) если на нем есть место. 
После запуска рабочих, запускается дочерний 
процесс-контролер, который снимает с конвейера деталь для 
проверки (есть ли такой идентификатор в списке процессов). 
Если рабочий не может выложить деталь за число тактов, 
равное числу рабочих (нет места на конвейере), он прерывает 
работу с жалобой на контролера. Если контролер не может 
взять с конвейера ни одной детали, он прерывает работу с 
жалобой на бригаду.*/