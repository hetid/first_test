#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
/*	argv[1] - количесво рабочих
*/
int rng(){
	int var;
	srand(getpid());
	int count = 3;
	for (int i = 0; i < count; ++i)
	{
		if (var == 0)
		{
			var = rand()%2;
		}
	}
	return(var);
}

struct msgbuf {
	long mtype;
	pid_t mtext;
};

int main(int argc, char* argv[]) {
	int countw = atoi(argv[1]);
	pid_t pids[countw], conv[countw];
	int stat;
	struct msgbuf buf;

	int msqid = msgget (IPC_PRIVATE, IPC_CREAT | 0600);
/*
	if (msqid == -1){
		printf ("\nmsgget завершился неудачей!\n");
		printf ("Код ошибки = %d\n", errno);
	}*/
	for (int i = 0; i < countw; ++i) {
		pids[i] = fork();
		if (pids[i] == 0) {
			int var = rng();
			pid_t a = 0;
			switch(var){
				case 0:
					printf("Рабочий %d [%d] не поместил деталь, жалоба на контролёра!\n", i+1, getpid());
					buf.mtype = 1;
					buf.mtext = a;
					msgsnd(msqid, &buf, sizeof(pid_t), 0);
					break;
				case 1:
					printf("Рабочий %d [%d] поместил деталь.\n", i+1, getpid());
					a = getpid();
					buf.mtype = 1;
					buf.mtext = a;
					msgsnd(msqid, &buf, sizeof(pid_t), 0);
					break;
			}
			exit(0);
		}
	}
	for (int i = 0; i < countw; ++i) {
		waitpid(pids[i], &stat, 0);
	}
	printf("Дочерние процессы завершены...\nПроцесс-контролёр запустился и получил детали:\n");
	pid_t kpid;
	kpid = fork();
	if (kpid == 0) {
		for (int i = 0; i < countw; ++i) {
			msgrcv(msqid, &buf, sizeof(pid_t), 0, IPC_NOWAIT);
			conv[i] = buf.mtext;
			if (conv[i] != 0)
			{
				printf("%d\n", conv[i]);
			}
		}
		int countk = 0;
		for (int i = 0; i < countw; ++i) {
			for (int j = 0; j < countw; ++j) {
				if (pids[j] == conv[i]) {
					countk++;
				}
			}
		}
		if (countk != countw)
		{
			printf("От контролёра поступила жалоба на бригаду.\n");
		}
		else printf("Жалоб на бригаду не поступило.\n");

		exit(0);
	}
	waitpid(kpid, &stat, 0);
	msgctl(msqid, IPC_RMID, 0);
	return 0;
}

/*Бригадный подряд. Родительский процесс создает заданное 
количество дочерних (условных рабочих), которые выкладывают 
на конвейер (в файл, глобальный массив) деталь (например, 
свой идентификатор) если на нем есть место. 
После запуска рабочих, запускается дочерний 
процесс-контролер, который снимает с конвейера деталь для 
проверки (есть ли такой идентификатор в списке процессов). 
Если рабочий не может выложить деталь за число тактов, 
равное числу рабочих (нет места на конвейере), он прерывает 
работу с жалобой на контролера. Если контролер не может 
взять с конвейера ни одной детали, он прерывает работу с 
жалобой на бригаду.*/