#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#define SIZESEGMENT 8
union semun {
	int val;                  
	struct semid_ds *buf;     
	unsigned short *array;
	struct seminfo *__buf;
};

int rng(){
	int var;
	srand(getpid());
	int count = 3;
	for (int i = 0; i < count; ++i)
	{
		if (var == 0)
		{
			var = rand()%2;
		}
	}
	return(var);
}

int main(int argc, char* argv[]) {
	int countw = atoi(argv[1]);
	pid_t pids[countw];
	for (int i = 0; i < countw; ++i) {
		pids[i] = 0;
	}
	int stat;

	//SHM
	int shmid = shmget(IPC_PRIVATE, SIZESEGMENT, IPC_CREAT | 0600);
	pid_t *ptr = shmat(shmid, 0, 0);
	for (int j = 0; j < countw; ++j) {
		ptr[j] = 0;
	}
	//SEM
	int semid = semget(IPC_PRIVATE, 1, IPC_CREAT | 0600);
	union semun arguments;
	arguments.val = 1;
	semctl(semid, 0, SETVAL, arguments);
	struct sembuf lock = {0, -1, 0};
	struct sembuf unlock = {0, 1, 0};

	for (int i = 0; i < countw; ++i) {
		pids[i] = fork();
		if (pids[i] == 0) {
			int var = rng();
			pid_t a = 0;
			//блокировка
			semop(semid, &lock, 1);
			
			switch(var){
				case 0:
					printf("Рабочий %d [%d] не поместил деталь, жалоба на контролёра!\n", i+1, getpid());					
					for (int i = 0; i < countw; ++i) {
						if (ptr[i] == 0) {
							ptr[i] = a;
							//разблокировка
							semop(semid, &unlock, 1);
							exit(0);
						}
					}

					break;
				case 1:
					printf("Рабочий %d [%d] поместил деталь.\n", i+1, getpid());
					a = getpid();					
					for (int i = 0; i < countw; ++i) {
						if (ptr[i] == 0) {
							ptr[i] = a;
							//разблокировка
							semop(semid, &unlock, 1);
							exit(0);
						}
					}
					break;
			}
		}
	}
	for (int i = 0; i < countw; ++i) {
		waitpid(pids[i], &stat, 0);
	}
	printf("Дочерние процессы завершены...\nПроцесс-контролёр запустился и получил детали:\n");
	pid_t kpid;
	kpid = fork();
	if (kpid == 0) {
		for (int i = 0; i < countw; ++i) {
			if (ptr[i] != 0)
			{
				printf("%d\n", ptr[i]);
			}
		}
		int countk = 0;
		for (int i = 0; i < countw; ++i) {
			for (int j = 0; j < countw; ++j) {
				if (pids[j] == ptr[i]) {
					countk++;
				}
			}
		}
		if (countk != countw)
		{
			printf("От контролёра поступила жалоба на бригаду.\n");
		}
		else printf("Жалоб на бригаду не поступило.\n");

		exit(0);
	}
	waitpid(kpid, &stat, 0);
	shmctl(shmid, IPC_RMID, 0);
	semctl(semid, 0, IPC_RMID);
	return 0;
}