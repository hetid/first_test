#define _SVID_SOURCE
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/msg.h>
#include "global.h"

pthread_mutex_t mutex;
int msqid;


void* udp_handler(void* arg);
void* tcp_handler(void* socket);
void sender_handler(int truesocket);
void reciever_handler(int truesocket);


int main(int argc, char const *argv[]){
	//создание очереди сообщений
	struct msgbuf buf;
	msqid = msgget (IPC_PRIVATE, IPC_CREAT | 0600);
	
	//Поток для UDP-общения
	pthread_t udp_tid;
	pthread_create(&udp_tid, 0, udp_handler, 0);

	//TCP сокет	
	int tcp_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in Sockaddr;
	Sockaddr.sin_family = AF_INET;
	Sockaddr.sin_port = htons(TCP_PORT);
	Sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	int val_true = 1;
	int i =0;
	if (setsockopt(tcp_socket, SOL_SOCKET, SO_REUSEADDR, &val_true, sizeof(int)) == -1) {
			close(tcp_socket);
			fprintf(stderr, "ERROR setsockopt\n");
			return -1;
	}
	
	bind(tcp_socket, (struct sockaddr*)(&Sockaddr), sizeof(Sockaddr));

	listen(tcp_socket, SOMAXCONN);

	pthread_t tcp_thread;

	while(1){
		int truesocket = accept(tcp_socket, 0, 0);
		printf("Соединение установлено!\n");
		pthread_create(&tcp_thread, NULL, tcp_handler, &truesocket);
	}	
	return 0;
}

void* udp_handler(void* arg){
	pthread_detach(pthread_self());

	struct udp_struct udp_msg;
	udp_msg.tcp_port = TCP_PORT;

	char msg_send[] = "Waiting for message";
	char msg_recieve[] = "Message exist";

	int UDP_socket1, UDP_socket2;
	struct msqid_ds qstatus;
	//создание UDP сокета
	UDP_socket1 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in UDP_addr1;
	UDP_addr1.sin_family = AF_INET;
	UDP_addr1.sin_port = htons(UDP_PORT1);
	UDP_addr1.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	UDP_socket2 = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in UDP_addr2;
	UDP_addr2.sin_family = AF_INET;
	UDP_addr2.sin_port = htons(UDP_PORT2);
	UDP_addr2.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	
	while(1)
	{
		msgctl (msqid, IPC_STAT, &qstatus);
		printf("\033[33mКол-во сообщений в очереди\033[0m = \033[31m%ld\n\033[0m", qstatus.msg_qnum);
		if (qstatus.msg_qnum < MAX_QUEUE_LEN)
		{	
			strcpy(udp_msg.message, msg_send);
			sendto(UDP_socket1, &udp_msg, sizeof(udp_msg), 0, (struct sockaddr *)&UDP_addr1, sizeof(UDP_addr1));
		}

		if (qstatus.msg_qnum > 0)
		{
			strcpy(udp_msg.message, msg_recieve);
			sendto(UDP_socket2, &udp_msg, sizeof(udp_msg), 0, (struct sockaddr *)&UDP_addr2, sizeof(UDP_addr2));
		}
		sleep(1);
	}
}

void* tcp_handler(void* socket){
	pthread_detach(pthread_self());
	int truesocket = *((int*)socket);
	struct msgbuf n;
	int type;
	recv(truesocket, &type, sizeof(int), MSG_NOSIGNAL);
	switch(type){
		case CLIENT_SENDER:
			sender_handler(truesocket);
			break;
		case CLIENT_RECIEVER:
			reciever_handler(truesocket);
			break;
		default: 
			close(truesocket);
			pthread_exit(NULL);
	}
}

void sender_handler(int truesocket){
	struct msgbuf n;
	struct msqid_ds qstatus;	//структура для статуса очереди

	while(1) {
		if (recv(truesocket, &n, sizeof(n), MSG_NOSIGNAL) < 0){
			pthread_exit(NULL);
		}

		printf("time =  %d ,len =  %d, %s\n\n", n.time, n.len, n.str);
		while(1) {
			pthread_mutex_lock(&mutex);

			msgctl (msqid, IPC_STAT, &qstatus);
			if (qstatus.msg_qnum == MAX_QUEUE_LEN) {
				pthread_mutex_unlock(&mutex);
				printf("QUEUE is full!\n");
				sleep(3);
				continue;
			}
			msgsnd(msqid, &n, sizeof(n), 0);
			printf("qstatus.msg_qnum = %ld\n", qstatus.msg_qnum); //////////////

			pthread_mutex_unlock(&mutex);
			break;
		}
	}
	close(truesocket);
	pthread_exit(NULL);

}

void reciever_handler(int truesocket){
	struct msgbuf recvmsg;
	struct msqid_ds qstatus;	//структура для статуса очереди

	while(1) {
		while(1) {
			pthread_mutex_lock(&mutex);
			
			msgctl (msqid, IPC_STAT, &qstatus);
			if (qstatus.msg_qnum == 0) {
				pthread_mutex_unlock(&mutex);
				printf("QUEUE is empty!\n");
				sleep(3);
				continue;
			}
			msgrcv(msqid, &recvmsg, sizeof(recvmsg), 0, IPC_NOWAIT);
			
			pthread_mutex_unlock(&mutex);
			break;
		}
		if (send(truesocket, &recvmsg, sizeof(recvmsg), MSG_NOSIGNAL) < 0){
			pthread_mutex_unlock(&mutex);
			pthread_exit(NULL);
		}
		printf("qstatus.msg_qnum = %ld\n", qstatus.msg_qnum);		///////////////
		sleep(recvmsg.time);
	}
	close(truesocket);
	pthread_exit(NULL);
}