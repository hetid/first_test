#!/bin/bash
create(){
	echo "Что создать?
		a - репозиторий
		b - ветку
		c - файл
		d - коммит"
		read upr
		case "$upr" in
			a)
				echo "Путь для создания репозитория:"
				read path
				git init $path
				echo "Локальный репозиторий создан!";
			;;
			b)
				echo "Путь до репозитория, в котором нужно создать ветку:"
				read path
				cd $path
				echo "Введите имя ветки:"
				read name
				git branch $name
				git branch
				echo "Ветка $name создана!";
			;;
			c)
				echo "Путь до репозитория, в котором нужно создать файл:"
				read path
				cd $path
				echo "Введите имя файла:"
				read name;
				>$name;
			;;
			d)
				echo "Путь до репозитория, в котором нужно создать коммит:"
				read path
				cd $path
				git status
				echo "Добавить индексацию? [y/n]"
				read word
				case $word in
					y )
						echo "Введите имена объектов (через пробел) для индексации:"
						read index
						git add $index
						echo "Введите комментарий к коммиту"
						read massage
						git commit -m "$massage"
						echo "Коммит создан!";
						;;
					n )
						echo "Введите комментарий к коммиту"
						read massage
						git commit -m "$massage"
						echo "Коммит создан!";
						;;
				esac
			;;
		esac
	./gits.sh;
}
edit(){
	echo "Путь до репозитория, в который нужно внести изменения:"
	read path
	cd $path;
	echo "Что редактировать?
		b - ветку
		c - коммит"
		read upr
		case "$upr" in
			b)
				git branch
				echo "Какую ветку редактировать?"
				read namebr
				git branch $namebr
				echo "Что сделать с веткой?
		1 - переименовать
		2 - merge"
				read word
				case $word in
					1)
						echo "Введите новое имя для ветки:"
						read newname
						git branch -b $newname
						git branch
						echo "Ветка переименована!";
						;;
					2)
						echo "Введите имя ветки для слияния:"
						read mname
						git merge HEAD $mname
						git branch
						echo "Слияние выполнено!";
						;;
				esac
			;;
			c)
				echo "Дополнить индексацию? [y/n]"
				read wor
				case $wor in
					y)
						git --amend;
						;;
					n)
						git --amend;
						;;
				esac
			;;	
		esac
	./gits.sh
}
delete(){
	echo "Что удалить?
		a - репозиторий
		b - ветку
		c - файл
		d - коммит"
		read upr
		case "$upr" in
			a)
				echo "Путь до репозитория, который нужно удалить:"
				read path
				cd $path
				rm -rf $path.git
				echo "Репозиторий удалён!";
			;;
			b)
				echo "Путь до репозитория, в котором нужно удалить ветку:"
				read path
				cd $path
				git branch
				echo "Какую ветку нужно удалить?"
				read delb
				git branch -d $delb
				git branch
				echo "Ветка $delb удалена!";
			;;
			c)
				echo "Путь до репозитория, в котором нужно удалить файл:"
				read path
				cd $path
				ll
				echo "Введите имя файла для удаления:"
				read fname
				rm -rf $path$fname
				ll
				echo "Файл $fname удалён!";
			;;
			d)
				echo "Путь до репозитория, в котором нужно удалить коммит:"
				read path
				cd $path
				echo "Имя ветки, в которой нужно удалить коммит:"
				read br
				git branch $br
				git --amend
				echo "Последний коммит удалён!";
			;;
		esac
}
pushpull(){
	echo "Что делать?
	1 -отправить
	2 -получить"
	read sq
	case $sq in
		1)
			echo "URL удалённого репозитория:"
			read url
			git remote add $url
			echo "Путь до локального репозитория:"
			read path
			cd $path
			git branch
			echo "Выберите ветку:"
			read bran
			git branch $bran
			git status
			echo "Добавить индексацию? [y/n]"
			read word
			case $word in
				y )
					echo "Введите имена объектов (через пробел) для индексации:"
					read index
					git add $index
					echo "Введите комментарий к коммиту"
					read massage
					git commit -m "$massage"
					echo "Коммит создан!";
					;;
				n )
					echo "Введите комментарий к коммиту"
					read massage
					git commit -m "$massage"
					echo "Коммит создан!";
					;;
			esac
			git push -u origin master
			echo "Коммит отправлен на удалённый репозиторий!";
			;;
		2)
			echo "Укажите директорию, в которую клонируется удалённый репозиторий:"
			read pathh
			cd $pathh
			echo "Введите url для клонирования удалённого репозитория:"
			read urll
			git clone $urll
			echo "Удалённый репозиторий склонирован";
			;;
	esac
	./gits.sh;
}
###################################################
echo "==========================
	Что нужно сделать?
a - создать
b - редактировать
c - удалить
d - отправить/получить
e - exit"
read upr
case "$upr" in
	a)
		create;		
	;;
	b)
		edit;	
	;;
	c)
		delete;
	;;
	d)
		pushpull;
	;;
	e)
		echo "Не очень-то и хотелось..."
		exit;
	;;
esac