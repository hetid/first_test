#include <stdio.h>
#include <stdlib.h>

struct debters {
	char sname[50];
	char date[50];
	char phone[50];
	long price;
}/*__attribute__((packed))*/;
typedef struct debters debt;

void reads(debt** a, int count){
	for (int i = 0; i < count; i++){
		a[i] = (debt*)malloc(sizeof(debt));
		printf("Введите фамилию должника %d\n", i+1);
		scanf("%s", (a[i])->sname);
		printf("Введите дату рождения должника %d (xx.xx.xxxx)\n", i+1);
		scanf("%s", (a[i])->date);
		printf("Введите номер телефона должника %d\n", i+1);
		scanf("%s", (a[i])->phone);
		printf("Введите величину задолжности должника %d\n", i+1);
		scanf("%ld", &((a[i])->price));
	}
}
void prints(debt** b, int count){
	for (int i = 0; i < count; ++i)
	{
		printf("%s\n", b[i]->sname);
		printf("%s\n", b[i]->date);
		printf("%s\n", b[i]->phone);
		printf("%ld\n", b[i]->price);
	}
}
void frees(debt** c, int count){
	for (int i = 0; i < count; i++){
		free(c[i]);
	}
	free(c);
}
/*static int cmp(const void *a, const void *b){
	debt * st1 = *(debt**)a;
	debt * st2 = *(debt**)b;
	return st2->price - st1->price;
}*/
static int cmp(const void *a, const void *b){
	debt * st1 = *(debt**)a;
	debt * st2 = *(debt**)b;
	return st2->price - st1->price;
}
int main(){
	int count;
	printf("Введите количество должников\n");
	scanf("%d", &count);
	debt **deb = (debt**)malloc(sizeof(debt**) * count);
	//попробовать объявить массив как аргумент в вызове reads 
	reads(deb, count);
	printf("====================================\n");

	qsort(deb, count, sizeof(debt*), cmp);

	prints(deb, count);
	frees(deb, count);
	return 0;
}