#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/file.h>
int fileno(FILE *stream);
int main(int argc, char* argv[]){
	int cu = atoi(argv[2]);
	int cgold = atoi(argv[3]);
	printf("В шахте %d золота.\n", cgold);
	int shift = atoi(argv[4]);
	int status, stts;
	int pid[cu];
	FILE *stream, *sstream;
	stream = fopen(argv[1], "w+");
	fprintf(stream, "%d \n", cgold);
	rewind(stream);
	fclose(stream);
	for (int i = 0; i < cu; ++i){
		pid[i] = fork();
		if (pid[i] == 0){
			int buf=0;
			srand(getpid());
			for (;;){
				printf("Юнит %d пошёл добывать. (PID=%d)\n", i+1, getpid());
				sleep(rand()%2); //Юнит задерживается в пути 0-2 сек.
				printf("Юнит %d начал добывать. (PID=%d)\n", i+1, getpid());
				sstream = fopen(argv[1], "r+");
				while(flock(fileno(sstream), LOCK_EX)) sleep(0.3);
				fscanf(sstream, "%d", &cgold);

				rewind(sstream);
				buf = cgold - shift;
				if (buf <= 0){
					printf("Нечего добывать. Шахта истощена!\n");
					sleep(rand()%2); //Юнит задерживается в пути 0-1 сек.
					printf("Юнит %d вернулся. (PID=%d)\n", i+1, getpid());
					exit(0);
				}
				fprintf(sstream, "%d \n", buf);

				rewind(sstream);
				flock(fileno(sstream), LOCK_UN);
				fclose(sstream);
				printf("Осталось золота: %d\n", buf);
				sleep(rand()%2); //Юнит задерживается в пути 0-1 сек.
				printf("Юнит %d вернулся. (PID=%d)\n", i+1, getpid());
			}
		}
	}
	for (int i = 0; i < cu; ++i){
		stts = waitpid(pid[i], &status, 0);
		if (pid[i] == stts){
			printf("Юнит %d завершил работу. (PID=%d)\n", i+1, getpid());
		}
	}
	return 0;
}