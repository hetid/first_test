#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
int main(int argc, char *argv[]){
	if (argc < 3){
		fprintf (stderr, "Мало аргументов. Укажите имена файлов при запуске\n");
		exit (1);
	}
	FILE *ffile = fopen(argv[1],"r");
	FILE *sfile = fopen("output", "w");
	char buf;
	while (!feof(ffile))
	{
		buf = getc(ffile);
		if (isalnum(buf))
		{
			fputc(buf, sfile);
		}
		else fputc(argv[2][0], sfile);
	}
	fclose(ffile);
	fclose(sfile); 
	return 0;
}