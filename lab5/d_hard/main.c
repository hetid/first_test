#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(int argc, char const *argv[])
{
	int a, b, var = 0;
	//Объявление указателя на функцию (одну из двух твоих)
	int (*hp)(int a, int b);
	//Заводим указатель, который будет хранить адрес
	//загруженной библиотеки librealdl.so
	void *lib_real;
	//записываем этот адрес
	lib_real = dlopen("/home/grisha/lessens/cproject/first_test/lab5/d_hard/librealdl.so", RTLD_LAZY);
	
	
	printf("================================\nВведите числа a и b \n");
	scanf("%d %d", &a, &b);
	printf("Числа: %d и %d\nКакую операцию выполнить:\n 1 - Деление по модулю\n 2 - Деление нацело\n", a, b);
	scanf("%d", &var);
	switch ( var ){
	case 1:
		//нужная функция из библиотеки на которую указывает 
		//lib_real задаётся   VV здесь своим именем 
		hp = dlsym(lib_real, "f1");
		//Теперь можем использовать конструкцию с указателем
		//на нужную функцию в принтф
		printf("Результат: %d\n", (*hp)(a, b));
	break;
	case 2:
		hp = dlsym(lib_real, "f2");
		printf("Результат: %d\n", (*hp)(a, b));
	break;
	}
	dlclose(lib_real);
	return 0;
}