#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#define MAX_LEN 256
void inner(char ***mass, int **kolstr)
{
	printf("Введите количество строк:\n");
	*kolstr = malloc(sizeof(int));
	scanf("%d", *kolstr);
	//printf("Значение в kolstr: %d\n", **kolstr);
	*mass=malloc(sizeof(char*) * (**kolstr));
	printf("Введите строки:\n");
	
	for (int i = 0; i < (**kolstr); ++i){
		(*mass)[i] = malloc(sizeof(char) * MAX_LEN);
		scanf("%s", (*mass)[i]);
	}
}
void outer(char **mas, int *kolstr){
	for (int i = 0; i < (*kolstr); ++i){
		printf("%s\n", mas[i]);
	}
}

void freee(char ***mas, int **kolstr){
	for (int i = 0; i < (**kolstr); ++i)
	{
		free((*mas)[i]);
	}
	free(*mas);
	free(*kolstr);
}

void swapmas(char **masfir, char **massec){
	char *buff;
	buff = *masfir;
	*masfir = *massec;
	*massec = buff;
}

int bubble(char **mas, int *kolstr){
	int cntr=0;
 //ПУЗЫРЁК
	for (int i = 0; i < (*kolstr) - 1; ++i){
		for (int j = 0; j < (*kolstr) - i -1; ++j){
			if (mas[j][0] < mas[j+1][0]){
				swapmas(&(mas[j]), &(mas[j+1]));
				cntr++;
			}	
		}
	}
	return cntr;
}

int main() {
	mtrace();
	char **pp;
	int *kk;
	int cntr = 0;
	inner(&pp, &kk);
	printf("	Неотсортированный массив: \n");
	outer(pp, kk);
	cntr = bubble(pp, kk);
	printf("	Количество перестановок: %d\n	Отсортированный в обратном алфавитном порядке массив: \n", cntr);
	outer(pp, kk);
	freee(&pp, &kk);
	return 0;
}