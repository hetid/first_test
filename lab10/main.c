#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

pthread_mutex_t mutex;
static int counter = 0;
struct data {
	int c;
	int* con;
};
typedef struct data data;
int rng(){
	int var = 0;
	srand(time(NULL));
	int count = 3;
	for (int i = 0; i < count; ++i) {
		if (var == 0) {
			var = rand()%2;
		}
	}
	return(var);
}

void* worker(void *args) {
	pthread_mutex_lock(&mutex);
	sleep(1);
	int var = rng();
	counter++;
	int a = counter;
	struct data *ldat = (data*)args; 
	int *local = ldat->con;

	switch(var){
		case 0:
			a = 0;
			printf("Рабочий [%d] не поместил деталь, жалоба на контролёра!\n", counter);
			local[counter-1] = a;
			pthread_mutex_unlock(&mutex);
			break;
		case 1:
			printf("Рабочий [%d] поместил деталь.\n", counter);					
			local[counter-1] = a;
			pthread_mutex_unlock(&mutex);
			break;
	}
	return NULL;
}

void* kont(void *args) {
	struct data *ldat = (data*)args;
	int *local = ldat->con;
	int counters = ldat->c;
	for (int i = 0; i < counters; ++i) {
		if (local[i] != i+1) {
			printf("От контролёра поступила жалоба на бригаду.\n");
		}
	}
}

int main(int argc, char* argv[]) {
	int countw = atoi(argv[1]);
	int conv[countw];
	struct data dat;
	dat.c = countw;
	dat.con = conv;
	for (int i = 0; i < countw; ++i) {
		conv[i] = 0;
	}

	pthread_t threads[countw];
	pthread_mutex_init(&mutex, NULL);

	for (int i = 0; i < countw; ++i) {
		pthread_create(&threads[i], NULL, worker, &dat);
	}
	for (int i = 0; i < countw; ++i) {
		pthread_join(threads[i], NULL);
	}
	pthread_mutex_destroy(&mutex);
	
/*	for (int i = 0; i < countw; ++i)
	{
		printf("%d\n", conv[i]);
	}
*/
	pthread_t kontrol;
	pthread_create(&kontrol, NULL, kont, &dat);
	pthread_join(kontrol, NULL);

	return 0;
}